package com.salesforce.tests.dependency;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The entry point for the Test program
 */
public class Main {

    public static void main(String[] args) {

        //read input from stdin
        Scanner scan = new Scanner(System.in);
        List<String> list = new ArrayList<>();
        List<String> mainList = new ArrayList<>();

        while (true) {
            String line = scan.nextLine();

            //no action for empty input

            if (line == null || line.length() == 0) {
                continue;
            }

            //the END command to stop the program
            if ("END".equals(line)) {
                System.out.println("END");
                break;
            }
            String[] arr = line.split(" ");
            List<String> argsList = new LinkedList<>(Arrays.asList(arr));
            argsList.remove(0);

            //Please provide your implementation here

            if (line.contains("DEPEND")) {
                for(int i=1; i<arr.length; i++) {
                    if(!list.contains(arr[i])) {
                        list.add(arr[i]);
                    }
                }
                String depName = argsList.get(0);

                Module current = Module.getInstance(depName);

                for (String strDependency : argsList.subList(1, argsList.size())) {
                    Module dependency = Module.getInstance(strDependency);
                    current.addDependency(dependency);
                    dependency.addDependent(current);
                }

            }

            if (line.contains("INSTALL")) {
                Module parent = Module.getInstance(argsList.get(0));
                if(parent != null) {
                    List<Module> mos = Module.isDependtIns(parent);
                    if(mos.size() > 0) {
                        for (Module m : mos
                        ) {
                            System.out.println("Installing " + m.getName());
                        }
                    } else {
                        System.out.println("Installing "+arr[1]);
                        list.add(arr[1]);
                    }
                }
            }

            if (line.contains("REMOVE")) {
                Module parent = Module.getInstance(argsList.get(0));
                if(parent != null) {
                    if(!Module.isDependt(parent)) {
                        System.out.println("Removing "+parent.getName());
                        list.remove(parent.getName());
                    }
                    else {
                        System.out.println(parent.getName() +" is still needed");
                    }
                }
            }

            if ("LIST".equals(line)) {
                for (String name: list){
                    System.out.println(name);
                }
            }

        }

    }
}