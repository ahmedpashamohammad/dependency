package com.salesforce.tests.dependency;

import java.util.*;

public class Module {
    protected static Map<String, Module> dependencyMap = new HashMap<String, Module>();

    private String name;
    private Set<Module> dependencies = new HashSet<Module>();
    private Set<Module> dependents = new HashSet<Module>();


    private Module(String name) {
        this.name = name;
    }



    public static Module getInstance(String name) {
        Module target = dependencyMap.get(name);
        if (target == null) {
            target = new Module(name);
            dependencyMap.put(name, target);
        }
        return target;
    }

    public static List<Module> isDependtIns(Module parent) {
        List<Module> ms = new ArrayList<>();
        Collection<Module> modules = dependencyMap.values();
        Iterator mods = modules.iterator();
        boolean flag = false;
        while(mods.hasNext()) {
            Module mod = (Module) mods.next();
            if(mod.getName().equalsIgnoreCase(parent.getName())) {
                ms.add(mod);
                flag = true;
                break;
            } else {
                ms.add(mod);
            }

        }
        if(flag)
            return ms;
        return Collections.emptyList();
    }

    public String getName() {
        return name;
    }


    public boolean addDependency(Module d) {
        return dependencies.add(d);
    }

    public boolean addDependent(Module d) {
        return dependents.add(d);
    }
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Module that = (Module) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;

        return true;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }


    public static Collection<Module> getAll() {
        return dependencyMap.values();
    }


    public static boolean isDependt(Module module) {
        Collection<Module> modules = dependencyMap.values();
        Iterator mods = modules.iterator();
        int i = 0;
        while(mods.hasNext()) {
            Module mod = (Module) mods.next();
            if(mod.getName().equalsIgnoreCase(module.getName()) && i ==0) {
                return false;
            }
                i++;
        }
        return true;
    }
}